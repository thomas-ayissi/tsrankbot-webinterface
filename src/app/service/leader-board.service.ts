import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Rest} from './entities/statsdb/transport';
import LeaderBoardSortType = Rest.LeaderBoardSortType;
import SortDirection = Rest.SortDirection;

@Injectable({
  providedIn: 'root'
})
export class LeaderBoardService {

  apiUrl = environment.statsDBApiUrl + 'leaderboard/recent/';

  constructor(private http: HttpClient) {
  }

  getLeaderboard(orderType: LeaderBoardSortType, sortDirection: SortDirection) {
    return this.http.get(this.apiUrl
      + '?sortType=' + orderType.toString().toLowerCase()
      + '&sortDirection=' + sortDirection.toString().toLowerCase());
  }
}
