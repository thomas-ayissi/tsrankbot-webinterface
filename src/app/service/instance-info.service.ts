import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {InstanceDetails} from './entities/instance-details';

@Injectable({
  providedIn: 'root'
})
export class InstanceInfoService {

  public instanceDetails: InstanceDetails;
  apiUrl = environment.rankBotApiUrl + 'instance/details';

  constructor(private http: HttpClient) {
    this.http.get(this.apiUrl).subscribe((data: InstanceDetails) => {
      this.instanceDetails = data;
      console.log('Finished loading instance details.');
    });
  }
}
