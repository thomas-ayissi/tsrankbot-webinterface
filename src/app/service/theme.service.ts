import {Inject, Injectable} from '@angular/core';
import {DOCUMENT} from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {
  private styleNames = {
    light: 'light-style.css',
    dark: 'dark-style.css'
  };
  private darkThemeActive: boolean;
  private localStorageDarkThemeEnabledKey = 'dark_theme_enabled';

  constructor(@Inject(DOCUMENT) private document: Document) {
  }

  public bootstrap() {
    const loadedValue = localStorage.getItem(this.localStorageDarkThemeEnabledKey);
    if (loadedValue == null) {
      this.setDarkTheme(true);
    } else {
      this.setDarkTheme(JSON.parse(loadedValue));
    }
  }

  setDarkTheme(isDarkTheme: boolean): void {
    this.darkThemeActive = isDarkTheme;
    this.loadStyle(isDarkTheme ? this.styleNames.dark : this.styleNames.light);
    localStorage.setItem(this.localStorageDarkThemeEnabledKey, JSON.stringify(isDarkTheme));
  }

  public isDarkModeActive() {
    return this.darkThemeActive;
  }


  loadStyle(styleName: string) {
    const head = this.document.getElementsByTagName('head')[0];

    const themeLink = this.document.getElementById(
      'selected-theme'
    ) as HTMLLinkElement;
    if (themeLink) {
      themeLink.href = styleName;
    } else {
      const style = this.document.createElement('link');
      style.id = 'selected-theme';
      style.rel = 'stylesheet';
      style.href = `${styleName}`;
      head.appendChild(style);
    }
  }
}
