import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {RainbowSixRank} from './entities/rainbow-six-rank';


@Injectable({
  providedIn: 'root'
})
export class RankService {

  apiUrl = environment.rankBotApiUrl + 'mmr/ranks';
  ranks = null;

  constructor(private http: HttpClient) {
    this.http.get(this.apiUrl).subscribe((data: RainbowSixRank[]) => {
      this.ranks = data;
    });
  }

  public getRanks(): RainbowSixRank[] {
    return this.ranks;
  }

  public buildRankImagePath(imageId: string): string {
    const path = 'assets/rank/' + imageId + '.svg';
    return path;
  }

  public getRankForMmr(mmr: number): RainbowSixRank {
    if (mmr == null) {
      return null;
    }
    for (const rank of this.getRanks()) {
      if (rank.mmrStart <= mmr) {
        return rank;
      }
    }
    return null;
  }

  public getDivisionByRank(rank: RainbowSixRank) {
    if (rank === null) {
      return null;
    }
    if (rank.aliasName.toLowerCase() === 'champion') {
      return rank.aliasName;
    }
    return rank.aliasName.substring(0, rank.aliasName.length - 2);
  }

  public getLowerRank(rank: RainbowSixRank): RainbowSixRank {
    if (rank === null) {
      console.error('Rank is null');
      return null;
    }
    let currentBestMatch = null;
    for (const currentRank of this.getRanks()) {
      if (currentRank.mmrStart < rank.mmrStart &&
        (currentBestMatch === null || currentRank.mmrStart > currentBestMatch.mmrStart)) {
        currentBestMatch = currentRank;
      }
    }
    return currentBestMatch;
  }

  public getUpperRank(rank: RainbowSixRank): RainbowSixRank {
    if (rank === null) {
      return null;
    }
    let currentBestMatch = null;
    for (const currentRank of this.getRanks()) {
      if (currentRank.mmrStart > rank.mmrStart) {
        currentBestMatch = currentRank;
      } else {
        break;
      }
    }
    return currentBestMatch;
  }

  /**
   * Calculates the required amount of matches to reach the next upper or lower rank
   *
   * @param mmr Players current MMR
   * @param elo Players amount of MMR change per game
   * @param upperRank calculate for lower rank if false, upper rank if true
   */
  public numberOfMatchesToNextRank(mmr: number, elo: number, upperRank: boolean): number {
    const eloGain = Math.abs(elo);
    const currentRank = this.getRankForMmr(mmr);
    const nextRank = this.getUpperRank(currentRank);
    if (nextRank === null) {
      return 0;
    }
    const goal = upperRank ? nextRank.mmrStart : (currentRank.mmrStart - 1);
    const requiredMatches = ((goal - mmr) / eloGain);
    return Math.abs(requiredMatches);
  }

  public convertVariationOffset(gamesToNextRank: number): number {
    const decimals = gamesToNextRank % 1;
    if (gamesToNextRank < 1) {
      // rank transition will occur for sure with next game
      return 0;
    }
    if (decimals === 0) {
      // Having no decimal places means our guess is on point.
      return 0;
    }
    if (decimals >= 0) {
      // Its likely an additional game is required
      return 1;
    }
    return 0;
  }

  public getClassForRank(rank: RainbowSixRank) {
    const division = this.getDivisionByRank(rank);
    if (division == null) {
      return '';
    }
    const divisionLower = division.toLowerCase();
    console.log(divisionLower);
    switch (divisionLower) {
      case 'champion':
        return 'channel-title-badge-rank-champion';
      case 'diamond':
        return 'channel-title-badge-rank-diamond';
      case 'platinum':
        return 'channel-title-badge-rank-platinum';
      case 'gold':
        return 'channel-title-badge-rank-gold';
      case 'silver':
        return 'channel-title-badge-rank-silver';
      case 'bronze':
        return 'channel-title-badge-rank-bronze';
      case 'copper':
        return 'channel-title-badge-rank-copper';
      default:
        return 'badge-default';
    }
  }
}
