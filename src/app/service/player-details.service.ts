import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Rest} from './entities/statsdb/transport';
import {Observable} from 'rxjs';
import DailyPlayerStatsTO = Rest.DailyPlayerStatsTO;
import MatchHistoryEntryTO = Rest.MatchHistoryEntryTO;
import ProfileOverviewTO = Rest.ProfileOverviewTO;
import KDHistoryTO = Rest.KDHistoryTO;
import RankByMmrTO = Rest.RankByMmrTO;
import RankedMmrHistoryTO = Rest.RankedMmrHistoryTO;
import MatchHistoryTO = Rest.MatchHistoryTO;
import KdarDailyHistoryTO = Rest.KdarDailyHistoryTO;

@Injectable({
  providedIn: 'root'
})
export class PlayerDetailsService {


  profileOverviewApi = environment.statsDBApiUrl + 'player/profile-overview/';
  dailyStatsApi = environment.statsDBApiUrl + 'player/daily-stats/';
  matchHistoryStatsApi = environment.statsDBApiUrl + 'player/match-history-stats/';
  kdrHistoryApi = environment.statsDBApiUrl + 'player/kd-history/';
  rankByMmrApi = environment.statsDBApiUrl + 'player/rank-by-mmr/';
  rankedMmrHistory = environment.statsDBApiUrl + 'player/mmr-history/';
  kdarDailyHistory = environment.statsDBApiUrl + 'player/kdar-history/';


  constructor(private http: HttpClient) {
  }

  getProfileOverview(region: string, uuid: string): Observable<ProfileOverviewTO> {
    return this.http.get<ProfileOverviewTO>(this.profileOverviewApi + region + '/' + uuid);
  }

  getDailyStats(region: string, uuid: string): Observable<DailyPlayerStatsTO> {
    return this.http.get<DailyPlayerStatsTO>(this.dailyStatsApi + region + '/' + uuid);
  }

  getMatchHistory(region: string, uuid: string, page: number): Observable<MatchHistoryTO> {
    return this.http.get<MatchHistoryTO>(this.matchHistoryStatsApi + region + '/' + uuid + '/' + page);
  }

  getKdrHistory(region: string, uuid: string): Observable<KDHistoryTO[]> {
    return this.http.get<KDHistoryTO[]>(this.kdrHistoryApi + region + '/' + uuid);
  }

  getRankByMmr(uuid: string): Observable<RankByMmrTO> {
    return this.http.get<RankByMmrTO>(this.rankByMmrApi + uuid);
  }

  getRankedMmrHistory(region: string, uuid: string): Observable<RankedMmrHistoryTO> {
    return this.http.get<RankedMmrHistoryTO>(this.rankedMmrHistory + region + '/' + uuid);
  }

  getKdarDailyHistory(region: string, uuid: string): Observable<KdarDailyHistoryTO> {
    return this.http.get<KdarDailyHistoryTO>(this.kdarDailyHistory + region + '/' + uuid);
  }

}
