/* tslint:disable */
/* eslint-disable */
// Generated using typescript-generator version 2.26.723 on 2021-04-23 20:18:00.

export namespace Rest {

  interface AdminAccountMappingEntryTO {
    internalAccountId: number;
    lastSnapshotId: number;
    teamspeakName: string;
    teamspeakUid: string;
    currentNickname: string;
    uplayUuid: string;
    accountAdded: Date;
    lastSnapshot: Date;
    lastSnapshotValidation: Date;
  }

  interface AdminAccountMappingResponseTO {
    entries: AdminAccountMappingEntryTO[];
    page: number;
    size: number;
    totalPages: number;
  }

  interface DailyPlayerStatsTO extends Serializable {
    firstMatch: Date;
    lastMatch: Date;
    rankedMatchesWon: number;
    rankedMatchesLost: number;
    rankedMatchesAbandoned: number;
    rankedKills: number;
    rankedDeaths: number;
    killAssists: number;
    headshotsChange: number;
    killDeathRatio: number;
    mmrChange: number;
    minMmr: number;
    maxMmr: number;
    dailyStitchDate: Date;
  }

  interface InstanceInfoTO {
    statsDBVersion: string;
    node: string;
  }

  interface KDHistoryTO {
    timeBlock: Date;
    minKdr: number;
    maxKdr: number;
    kdr: number;
    snapshotsInBucket: number;
  }

  interface KdarDailyHistoryEntryTO {
    day: string;
    dailyKills: number;
    dailyDeaths: number;
    dailyAssists: number;
    kdar: number;
  }

  interface KdarDailyHistoryTO {
    uuid: string;
    entries: KdarDailyHistoryEntryTO[];
  }

  interface LeaderBoardEntryTO extends Serializable {
    accountInternalAccountId: number;
    playerName: string;
    uuid: string;
    rankedKillsCurrent: number;
    rankedKillsMin: number;
    rankedKillsAvg: number;
    rankedKillsSum: number;
    rankedDeathsMin: number;
    rankedDeathsAvg: number;
    rankedDeathsMax: number;
    rankedDeathsSum: number;
    assistsCurrent: number;
    assistsMin: number;
    assistsAvg: number;
    assistsMax: number;
    assistsSum: number;
    rankedWlRate: number;
    mmrMin: number;
    mmrAvg: number;
    mmrMax: number;
    mmrChangeSum: number;
    levelDelta: number;
  }

  interface LeaderBoardTO extends Serializable {
    entries: LeaderBoardEntryTO[];
    sortType: LeaderBoardSortType;
    sortDirection: SortDirection;
  }

  interface MatchHistoryEntryTO extends Serializable {
    snapshotId: number;
    snapshotCreatedDate: Date;
    matchesWonChange: number;
    matchesLostChange: number;
    matchesAbandonedChange: number;
    multipleMatches: boolean;
    killsChange: number;
    deathsChange: number;
    killAssistsChange: number;
    headshotsChange: number;
    killDeathRatio: number;
    rankId: number;
    mmrCurrent: number;
    mmrChange: number;
    revives: number;
    revivesDenied: number;
    dbnos: number;
    dbnoAssists: number;
    suicides: number;
    gadgetsDestoryed: number;
    barricadesDeployed: number;
    distanceTraveled: number;
  }

  interface MatchHistoryTO extends Serializable {
    entries: MatchHistoryEntryTO[];
    currentPage: number;
    totalPages: number;
    itemsPerPage: number;
  }

  interface ProfileOverviewTO {
    region: string;
    uuid: string;
    currentName: string;
    clearanceLevel: number;
    maxRank: number;
    maxMmr: number;
    mmr: number;
    wins: number;
    losses: number;
    abandons: number;
    season: number;
    rankedKills: number;
    rankedDeaths: number;
    rankedAssists: number;
    rankedKdr: number;
    bulletsFired: number;
    bulletsHit: number;
    penetrationKills: number;
    headshots: number;
    resultSource: SnapshotFetchResultSource;
  }

  interface RankByMmrTO {
    internalAccountId: number;
    uuid: string;
    rank: number;
    name: string;
    mmr: number;
    snapshotDate: Date;
  }

  interface RankedMmrHistoryTO {
    accountId: number;
    uuid: string;
    region: string;
    entries: RankedMmrHistoryEntryTO[];
  }

  interface Serializable {
  }

  interface RankedMmrHistoryEntryTO {
    snapshotId: number;
    snapshotDate: Date;
    mmr: number;
  }

  type LeaderBoardSortType = "MMR" | "MMR_CHANGE" | "RANKED_KD_RATE" | "RANKED_WL_RATE";

  type SortDirection = "ASC" | "DESC";

  type SnapshotFetchResultSource = "API_FETCH_ERROR_USING_LATEST_SNAPSHOT" | "SNAPSHOT_CREATED" | "SNAPSHOT_VALIDATED" | "SNAPSHOT_STILL_VALID" | "SNAPSHOT_UP_TO_DATE";

}
