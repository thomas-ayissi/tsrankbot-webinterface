import {RainbowSixRank} from './rainbow-six-rank';
import {SnapshotFetchResultSource} from './snapshot-fetch-result-source';
import {TsClientSnapshotHistory} from './ts-client-snapshot-history';

export interface UplayAccount {
  clearanceLevel: number;
  mmr: number;
  hasRank: boolean;
  rainbowSixRank: RainbowSixRank;
  uplayUuid: string;
  uplayName: string;
  rankedMatchWins: number;
  rankedMatchLosses: number;
  rankedMatchCount: number;
  rankedMatchAbandons: number;
  rankedKdr: number;
  lastMmrChange: number;
  resultSource: string; // Use object SnapshotFetchResultSource
  lastRankedKdrChange: number;
  lastRankedMatchAmountChange: number;
  snapshotHistory: TsClientSnapshotHistory[];

  // Fields starting with underscore (_) are filled by the webapp
  _mmrDifference: number;
  _hasMinimumClearanceLevel: boolean;
  _isMainAccount: boolean;
  _isLeaver: boolean;
  _leaverPercentage: number;
  _resultSourceType: SnapshotFetchResultSource;
  _recentlyUpdated: boolean;
}
