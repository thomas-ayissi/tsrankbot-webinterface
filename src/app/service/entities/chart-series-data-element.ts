export class ChartSeriesDataElement {
  name: string;
  value: number;
  min?: number;
  max?: number;
}
