/* eslint-disable */
export enum SnapshotFetchResultSource {
  API_FETCH_ERROR_USING_LATEST_SNAPSHOT,
  SNAPSHOT_CREATED,
  SNAPSHOT_VALIDATED,
  SNAPSHOT_STILL_VALID,
  SNAPSHOT_UP_TO_DATE
}
