export interface RainbowSixRank {
  id: number;
  aliasName: string;
  mmrStart: number;
  imageId: string;
}
