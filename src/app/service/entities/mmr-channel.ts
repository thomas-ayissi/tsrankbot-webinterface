import {Client} from './client';

export interface MmrChannel {
  clients: Client[];
  calculatedMmr: number;
  channelName: string;
  calculatedMmrGap: number;

  // Fields starting with underscore (_) are filled by the webapp
  _calculatedMainAccountMmrGap: number;
  _calculatedMmrGapExceeded: boolean;
  _calculatedMmrGapWarning: boolean;
  _hasClientsWithUnsatisfiedClearanceLevel: boolean;
  _mmrAverage: number;
  _hasLeaver: boolean;
}
