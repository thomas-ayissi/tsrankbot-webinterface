/* eslint-disable */
export class InstanceDetails {
  public backendVersion: String;

  public tsServerName: String;

  public maxMmrGapRanked: number;

  public minRankedClearanceLevel: number;
}
