import {Injectable} from '@angular/core';
import {UplayAccount} from './entities/uplay-account';

@Injectable({
  providedIn: 'root'
})
export class MmrCompareService {

  public selectedAccounts: UplayAccount[] = [];

  constructor() {
  }


  /**
   * Adds or removes an account from the compare list.
   *
   * @param account Account to add / remove.
   */
  public addOrRemove(account: UplayAccount) {
    if (this.isInCompare(account)) {
      this.removeFromCompare(account);
    } else {
      this.addToCompare(account);
    }
  }

  /**
   * Addds an account to the compare list.
   *
   * @param account Accout to add
   * @return true when account was added, false when already in list
   */
  public addToCompare(account: UplayAccount): boolean {
    if (this.selectedAccounts !== undefined) {
      for (const a of this.selectedAccounts) {
        if (a.uplayUuid === account.uplayUuid) {
          // Account is already selected for compare
          return false;
        }
      }
    }
    this.selectedAccounts.push(account);
    return true;
  }

  /**
   * Removes an account from the compare list.
   *
   * @param account Account to remove
   * @return true when removed, false when not found and therefore not removed
   */
  public removeFromCompare(account: UplayAccount) {
    if (this.selectedAccounts === undefined) {
      return false;
    }
    for (let i = 0; i < this.selectedAccounts.length; i++) {
      const a = this.selectedAccounts[i];
      if (a.uplayUuid === account.uplayUuid) {
        this.selectedAccounts.splice(i, 1);
        return true;
      }
    }
    return false;
  }

  public removeAll() {
    this.selectedAccounts = [];
  }

  /**
   * Check if the given account is already in the compare list.
   *
   * @param account Account to check
   * @return true when found in compare list, false when not.
   */
  public isInCompare(account: UplayAccount): boolean {
    if (this.selectedAccounts === undefined) {
      return false;
    }
    for (const a of this.selectedAccounts) {
      if (a === undefined) {
        continue;
      }
      if (a.uplayUuid === account.uplayUuid) {
        return true;
      }
    }
    return false;
  }

  public getSelectedAccountsString() {
    let out = '';
    let first = true;
    for (const account of this.selectedAccounts) {
      if (!first) {
        out += ', ';
      } else {
        first = false;
      }
      out += account.uplayName;
    }
    return out;
  }

  /**
   */
  public calculateMmrDifference(): number {
    if (this.selectedAccounts.length === 0) {
      return null;
    }

    let min: number = null;
    let max: number = null;
    for (const account of this.selectedAccounts) {
      if (min === null || account.mmr < min) {
        min = account.mmr;
      }
      if (max === null || account.mmr > max) {
        max = account.mmr;
      }
    }
    return max - min;
  }

  public calculateMmrAverage(): number {
    if (this.selectedAccounts.length === 0) {
      return null;
    }
    let mmrSum = 0;
    for (const acc of this.selectedAccounts) {
      mmrSum += acc.mmr;
    }

    return mmrSum / this.selectedAccounts.length;
  }

  /**
   * Removes all accounts that are not provided with the list and updates internal cache with new data.
   *
   * @param uplayAccounts accounts which are valid.
   */
  public validateAccounts(uplayAccounts: UplayAccount[]) {
    for (let i = 0; i < this.selectedAccounts.length; i++) {
      const cached = this.selectedAccounts[i];
      let stillValid = false;
      for (const toValidate of uplayAccounts) {
        if (cached.uplayUuid === toValidate.uplayUuid) {
          stillValid = true;
          this.selectedAccounts[i] = toValidate;
          break;
        }
      }
      // Remove accounts which are no longer valid
      if (!stillValid) {
        this.removeFromCompare(cached);
      }
    }
  }
}
