import {Component, OnInit, ViewChild} from '@angular/core';
import {LeaderBoardService} from '../../../service/leader-board.service';
import {NgxSpinnerService} from 'ngx-spinner';
import {Rest} from '../../../service/entities/statsdb/transport';
import {ActivatedRoute, Router} from '@angular/router';
import {PlayerOverviewComponent} from '../../../components/player-overview/player-overview.component';
import SortDirection = Rest.SortDirection;
import LeaderBoardSortType = Rest.LeaderBoardSortType;
import LeaderBoardTO = Rest.LeaderBoardTO;

const SPINNER_NAME_LEADERBOARD = 'spinner-load-leaderboard';

@Component({
  selector: 'app-recent-leader-board',
  templateUrl: './recent-leader-board.component.html',
  styleUrls: ['./recent-leader-board.component.css']
})
export class RecentLeaderBoardComponent implements OnInit {

  @ViewChild(PlayerOverviewComponent) playerOverviewChild: PlayerOverviewComponent;


  public leaderBoardData: LeaderBoardTO = null;
  public loadingData = false;
  public activeSortType: LeaderBoardSortType;
  public activeSortDirection: SortDirection;


  constructor(private leaderBoardService: LeaderBoardService,
              private spinner: NgxSpinnerService,
              private router: Router,
              private activatedRouteSnapshot: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.loadingData = true;
    this.activatedRouteSnapshot.queryParams.subscribe(params => {
      this.activeSortType = params.sortType === undefined ? 'MMR' : params.sortType.toUpperCase();
      this.activeSortDirection = params.sortDirection === undefined ? 'DESC' : params.sortDirection.toUpperCase();
      this.loadData();
    });
  }

  loadData() {
    this.loadingData = true;
    setTimeout(() => {
      this.spinner.show(SPINNER_NAME_LEADERBOARD, {
        type: 'line-scale-party',
        bdColor: 'rgba(127,127,127,0.56)',
        fullScreen: false
      });
    }, 0);
    return this.leaderBoardService.getLeaderboard(this.activeSortType, this.activeSortDirection).subscribe((data: LeaderBoardTO) => {
      this.leaderBoardData = data;
      this.loadingData = false;
      this.spinner.hide(SPINNER_NAME_LEADERBOARD);
    });
  }

  /**
   * Switches the active sort to the new desired type or alternates the sort direction.
   *
   * @param desiredType new type to sort by
   */
  toggleSort(desiredType: LeaderBoardSortType) {
    let newSortType: LeaderBoardSortType = this.activeSortType;
    let newSortDirection: SortDirection = this.activeSortDirection;
    if (this.activeSortType === desiredType) {
      if (this.activeSortDirection === 'ASC') {
        newSortDirection = 'DESC';
      } else {
        newSortDirection = 'ASC';
      }
    } else {
      newSortType = desiredType;
    }

    this.router.navigate([], {
      queryParams: {
        sortType: newSortType.toLocaleLowerCase(),
        sortDirection: newSortDirection.toLocaleLowerCase()
      }
    });
  }

  public openDetails(uuid: string) {
    this.playerOverviewChild.openModal('euw', uuid);
  }


}
