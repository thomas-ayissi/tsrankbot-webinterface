import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {RecentLeaderBoardComponent} from './recent-leader-board.component';

describe('RecentLeaderBoardComponent', () => {
  let component: RecentLeaderBoardComponent;
  let fixture: ComponentFixture<RecentLeaderBoardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RecentLeaderBoardComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecentLeaderBoardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
