import {Component, OnInit} from '@angular/core';
import packageInfo from '../../../../package.json';
import {RankService} from '../../service/rank.service';
import {RainbowSixRank} from '../../service/entities/rainbow-six-rank';
import {StatsDbInstanceServiceService} from '../../service/stats-db-instance-service.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  public frontEndVersion: string = packageInfo.version;
  lastDivision: string = null;

  constructor(
    public rankService: RankService, public statsDbInstanceService: StatsDbInstanceServiceService) {
  }

  ngOnInit() {
  }

  isNewDivision(rank: RainbowSixRank): boolean {
    const division = this.getDivisionByRank(rank);
    if (division !== this.lastDivision) {
      this.lastDivision = division;
      return true;
    } else {
      return false;
    }
  }

  buildTooltip(rank: RainbowSixRank): string {
    const upperRank = this.rankService.getUpperRank(rank);
    let upTo = '+';
    if (upperRank !== null) {
      upTo = ' - ' + (upperRank.mmrStart - 1);
    }
    return rank.aliasName + ': ' + rank.mmrStart + upTo + ' MMR';
  }

  private getDivisionByRank(rank: RainbowSixRank) {
    return this.rankService.getDivisionByRank(rank);
  }

}
