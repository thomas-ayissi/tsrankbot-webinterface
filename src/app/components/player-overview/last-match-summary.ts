export class LastMatchSummary {
  public countedMatches = 0;
  public won = 0;
  public lost = 0;
  public kills = 0;
  public deaths = 0;
  public donuts = 0;
  public doubleDigits = 0;
  public flawless = 0;
  public kdr = 0;

}
