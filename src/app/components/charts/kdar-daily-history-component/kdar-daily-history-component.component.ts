import {Component, Input, OnInit} from '@angular/core';
import * as dayjs from 'dayjs';
import * as dayjsEnGb from 'dayjs/locale/en-gb';
import * as dayjsWeekOfYear from 'dayjs/plugin/weekOfYear';
import * as dayjsWeekDay from 'dayjs/plugin/weekday';

import {Rest} from '../../../service/entities/statsdb/transport';
import KdarDailyHistoryEntryTO = Rest.KdarDailyHistoryEntryTO;


const kdarLookbackDays = 7 * 4 * 3;

@Component({
  selector: 'app-kdar-daily-history-component',
  templateUrl: './kdar-daily-history-component.component.html',
  styleUrls: ['./kdar-daily-history-component.component.css']
})
export class KdarDailyHistoryComponentComponent implements OnInit {


  @Input() kdarDailyHistoryEntry: KdarDailyHistoryEntryTO[];

  // options
  scheme = 'dark';
  legend = true;
  xAxis = true;
  yAxis = true;
  showYAxisLabel = true;
  showXAxisLabel = true;
  xAxisLabel = 'Calender Week';
  yAxisLabel = 'KDA Ratio';
  legendPosition = 'right';
  yAxisTicks = [];
  colorScheme = {
    domain: ['rgba(241,6,6,0.84)', 'rgba(199,86,7,0.9)', 'rgba(220,217,13,0.9)', 'rgba(26,184,158,0.85)',
      'rgba(27,189,11,0.75)', 'rgba(19,232,14,0.93)', 'rgb(19,232,14)']
  };
  chartData = [];
  legendTitle = 'Irgenwat';
  innerPadding = 2;
  maxKdarValue = 2.5;

  /**
   * This is used as kdar when the user has not played any matches on this day.
   * We use this because the ngx-charts framework is unable to render null / empty values,
   * but we need to display days also when the user has not played games on these days.
   *
   */
  public readonly _noMatchKdar = -999;

  constructor() {
    dayjs.locale(dayjsEnGb);
    dayjs.extend(dayjsWeekOfYear);
    dayjs.extend(dayjsWeekDay);
  }

  public humanReadableWeekday(value) {
    return dayjs(value).format('dd');
  }

  public dayOfWeek(value): number {
    return Number(dayjs(value).format('d'));
  }

  public weekOfYear(value) {
    return dayjs(value).week();
  }

  public formatY(value) {
    return value;
  }

  ngOnInit(): void {

    // today - <90> == first entry
    const today = new Date();
    const lastDayOfThisWeek = dayjs(today).endOf('week');
    // FIXME load from backend (90)
    let lastCalendarWeek = null;

    const ticks = [];
    for (let i = 6; i >= 0; i--) {
      ticks.push(dayjs(lastDayOfThisWeek).subtract(i, 'day').format('dd'));
    }
    this.yAxisTicks = ticks;

    for (let dayCount = kdarLookbackDays; dayCount >= 0; dayCount--) {

      const selectedDay = dayjs(lastDayOfThisWeek).subtract(dayCount, 'day');
      const weekOfYear = this.weekOfYear(selectedDay);

      if (lastCalendarWeek === null || lastCalendarWeek !== weekOfYear) {
        lastCalendarWeek = weekOfYear;

        const weekData = [];

        for (let currentDayOfWeek = 0; currentDayOfWeek < 7; currentDayOfWeek++) {

          const weekDay = dayjs().weekday(currentDayOfWeek);

          let cEntity = null;
          for (const kdarEntry of this.kdarDailyHistoryEntry) {
            if (this.dayOfWeek(kdarEntry.day) === currentDayOfWeek
              && this.weekOfYear(kdarEntry.day) === weekOfYear) {
              cEntity = kdarEntry;
              break;
            }
          }
          weekData.push({
            name: weekDay.format('dd'),
            value: cEntity === null ? this._noMatchKdar : cEntity.kdar,
            entityDate: cEntity === null ? 0 : cEntity.day,
          });
        }

        const calenderWeekEntry = {
          name: weekOfYear,
          series: weekData.reverse()
        };
        this.chartData.push(calenderWeekEntry);
      }
    }
  }
}
