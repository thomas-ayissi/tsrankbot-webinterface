import {Component, Input, OnInit} from '@angular/core';
import {ChartSeriesDataElement} from '../../../service/entities/chart-series-data-element';
import * as dayjs from 'dayjs';

@Component({
  selector: 'app-k-d-a-chart',
  templateUrl: './k-d-a-chart.component.html',
  styleUrls: ['./k-d-a-chart.component.scss']
})
export class KDAChartComponent implements OnInit {

  @Input() killData: ChartSeriesDataElement[];
  @Input() assistsData: ChartSeriesDataElement[];
  @Input() deathData: ChartSeriesDataElement[];
  @Input() kdrData: ChartSeriesDataElement[];

  scheme = 'dark';
  // options
  legend = true;
  showLabels = true;
  animations = true;
  xAxis = true;
  yAxis = true;
  showYAxisLabel = true;
  showXAxisLabel = true;
  xAxisLabel = 'Time';
  yAxisLabel = 'KDR';
  timeline = false;
  legendPosition = 'right';
  colorScheme = {
    domain: ['rgba(8,173,217,0.84)', 'rgba(205,13,172,0.85)', 'rgba(205,6,6,0.86)', 'rgba(0,255,7,0.86)']
  };
  chartData = undefined;

  constructor() {

  }

  ngOnInit(): void {
    this.chartData = [
      {
        name: 'Kills',
        series: this.killData
      },
      {
        name: 'Assists',
        series: this.assistsData
      },
      {
        name: 'Deaths',
        series: this.deathData
      },
      {
        name: 'KDR',
        series: this.kdrData
      }
    ];
  }

  public formatDate(value) {
    return dayjs(value).format('MM/DD');
  }
}
