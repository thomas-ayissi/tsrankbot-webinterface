import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {RankedMmrHistoryChartComponent} from './ranked-mmr-history-chart.component';

describe('RankedMmrHistoryComponent', () => {
  let component: RankedMmrHistoryChartComponent;
  let fixture: ComponentFixture<RankedMmrHistoryChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RankedMmrHistoryChartComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RankedMmrHistoryChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
