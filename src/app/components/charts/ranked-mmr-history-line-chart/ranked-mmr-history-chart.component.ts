import {Component, Input, OnInit} from '@angular/core';
import {ChartSeriesDataElement} from '../../../service/entities/chart-series-data-element';
import {curveMonotoneX} from 'd3-shape';
import * as dayjs from 'dayjs';
import {RankService} from '../../../service/rank.service';

@Component({
  selector: 'app-ranked-mmr-history-chart',
  templateUrl: './ranked-mmr-history-chart.component.html',
  styleUrls: ['./ranked-mmr-history-chart.component.css']
})
export class RankedMmrHistoryChartComponent implements OnInit {

  @Input() mmrHistoryData: ChartSeriesDataElement[];

  scheme = 'dark';
  // options
  legend = false;
  showLabels = true;
  animations = true;
  xAxis = true;
  yAxis = true;
  showYAxisLabel = true;
  showXAxisLabel = true;
  xAxisLabel = 'Time';
  yAxisLabel = 'MMR';
  timeline = true;
  legendPosition = 'right';
  showGridLines = false;
  rotateXAxisTicks = true;
  curve: any = curveMonotoneX;
  colorScheme = {
    domain: ['rgba(8,173,217,0.84)']
  };
  chartData = undefined;
  referenceLines = [];
  yScaleMin = null;
  yScaleMax = null;

  constructor(private rankService: RankService) {

  }


  public formatDate(value) {
    return dayjs(value).format('MM/DD');
  }


  ngOnInit(): void {
    this.chartData = [
      {
        name: 'MMR',
        series: this.mmrHistoryData
      },
    ];

    this.initRefLinesAndMinMax();
  }

  /**
   * This defines the reference lines based on ranks we know.
   * min/max is calculated by taking the lowest/highest point in our dataset and extending it by one entire rank.
   * As result we get +1 and -1 rank as padding
   *
   * @private
   */
  private initRefLinesAndMinMax() {
    // Find top low and top high in our results dataset.
    let min = null;
    let max = null;
    for (const d of this.mmrHistoryData) {
      if (min === null || min > d.value) {
        min = d.value;
      }
      if (max === null || max < d.value) {
        max = d.value;
      }
    }

    // Find next lower rank.
    const lowerRank = this.rankService.getLowerRank(this.rankService.getRankForMmr(min));
    if (lowerRank !== null) {
      min = lowerRank.mmrStart - 1;
    }
    // Find next higher rank.
    const upperRank = this.rankService.getUpperRank(this.rankService.getRankForMmr(max));
    if (upperRank !== null) {
      max = upperRank.mmrStart + 1;
    }


    // Kick out all reference lines which are exceeding the min or max range,
    // ngx-charts can't deal with ref lines which are out of min/max.
    // It would render them out of bounds.
    const newRefLines = [];
    for (const rank of this.rankService.getRanks()) {
      if (rank.mmrStart < max && rank.mmrStart > min) {
        newRefLines.push({value: rank.mmrStart, name: rank.aliasName});
      }
    }

    // Write our findings back to the view...
    this.referenceLines = newRefLines;
    this.yScaleMax = max;
    this.yScaleMin = min;
  }
}
