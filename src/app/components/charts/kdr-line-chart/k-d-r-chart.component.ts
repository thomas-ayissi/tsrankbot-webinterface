import {Component, Input, OnInit} from '@angular/core';
import {ChartSeriesDataElement} from '../../../service/entities/chart-series-data-element';
import * as dayjs from 'dayjs';

@Component({
  selector: 'app-k-d-r-chart',
  templateUrl: './k-d-r-chart.component.html',
  styleUrls: ['./k-d-r-chart.component.scss']
})
export class KDRChartComponent implements OnInit {

  @Input() kdData: ChartSeriesDataElement[];
  scheme = 'dark';
  // options
  legend = true;
  showLabels = true;
  animations = true;
  xAxis = true;
  yAxis = true;
  showYAxisLabel = true;
  showXAxisLabel = true;
  xAxisLabel = 'Time';
  yAxisLabel = 'KDR';
  timeline = false;
  legendPosition = 'right';
  colorScheme = {
    domain: ['rgba(8,173,217,0.84)', 'rgba(205,13,172,0.85)', 'rgba(205,6,6,0.86)', '#b701e2', '#a8385d', '#aae3f5']
  };

  chartData = undefined;

  constructor() {

  }

  ngOnInit(): void {
    this.chartData = [
      {
        name: 'KDR',
        series: this.kdData
      },
    ];
  }

  public formatDate(value) {
    return dayjs(value).format('MM/DD');
  }
}
