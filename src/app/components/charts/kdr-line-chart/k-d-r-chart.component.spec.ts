import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {KDRChartComponent} from './k-d-r-chart.component';

describe('KdrGraphComponent', () => {
  let component: KDRChartComponent;
  let fixture: ComponentFixture<KDRChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [KDRChartComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KDRChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
