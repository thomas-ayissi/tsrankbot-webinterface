import {BrowserModule} from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {TeamspeakChannelOverviewComponent} from './pages/teamspeak-channel-overview/teamspeak-channel-overview.component';
import {MmrService} from './service/mmr.service';
import {HttpClientModule} from '@angular/common/http';
import {LoadingBarHttpClientModule} from '@ngx-loading-bar/http-client';
import {FooterComponent} from './components/footer/footer.component';
import {InstanceInfoService} from './service/instance-info.service';
import {RankService} from './service/rank.service';
import {BsDropdownModule} from 'ngx-bootstrap/dropdown';
import {TooltipModule} from 'ngx-bootstrap/tooltip';
import {ModalModule} from 'ngx-bootstrap/modal';
import {PlayerOverviewComponent} from './components/player-overview/player-overview.component';
import {KDAChartComponent} from './components/charts/kda-line-chart/k-d-a-chart.component';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {RankTransitionComponent} from './components/rank-transition/rank-transition.component';
import {ThemeService} from './service/theme.service';
import {rxStompConfig} from './rx-stomp.config';
import {InjectableRxStompConfig, RxStompService, rxStompServiceFactory} from '@stomp/ng2-stompjs';
import {NgxSpinnerModule} from 'ngx-spinner';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FaIconLibrary, FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {faInfoCircle} from '@fortawesome/free-solid-svg-icons/faInfoCircle';
import {KDRChartComponent} from './components/charts/kdr-line-chart/k-d-r-chart.component';
import {DayOnlyDatePipePipe} from './pipes/day-only-date-pipe';
import {faArrowLeft} from '@fortawesome/free-solid-svg-icons/faArrowLeft';
import {RouterModule, Routes} from '@angular/router';
import {LeaderBoardComponent} from './pages/leader-board/leader-board.component';
import {HashLocationStrategy, LocationStrategy} from '@angular/common';
import {RecentLeaderBoardComponent} from './pages/leader-board/recent-leader-board/recent-leader-board.component';
import {OverallLeaderBoardComponent} from './pages/leader-board/overall-leader-board/overall-leader-board.component';
import {RankedMmrHistoryChartComponent} from './components/charts/ranked-mmr-history-line-chart/ranked-mmr-history-chart.component';
import {DayHourMinutePipe} from './pipes/day-hour-minute-pipe';
import {ToastrModule} from 'ngx-toastr';
// eslint-disable-next-line max-len
import { KdarDailyHistoryComponentComponent } from './components/charts/kdar-daily-history-component/kdar-daily-history-component.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'ts-channel-overview',
    pathMatch: 'full'
  },
  {
    path: 'ts-channel-overview',
    component: TeamspeakChannelOverviewComponent
  },
  {
    path: 'leaderboard',
    component: LeaderBoardComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'recent'
      },
      {
        path: 'recent',
        component: RecentLeaderBoardComponent
      },
      {
        path: 'overall',
        component: OverallLeaderBoardComponent
      }
    ]
  }
];

@NgModule({
  declarations: [
    AppComponent,
    TeamspeakChannelOverviewComponent,
    FooterComponent,
    PlayerOverviewComponent,
    KDAChartComponent,
    RankTransitionComponent,
    KDRChartComponent,
    LeaderBoardComponent,
    RecentLeaderBoardComponent,
    OverallLeaderBoardComponent,
    RankedMmrHistoryChartComponent,
    KdarDailyHistoryComponentComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
    LoadingBarHttpClientModule,
    BsDropdownModule.forRoot(),
    TooltipModule.forRoot(),
    ModalModule.forRoot(),
    NgxChartsModule,
    NgxSpinnerModule,
    BrowserAnimationsModule,
    FontAwesomeModule,
    ToastrModule.forRoot()
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [ThemeService, MmrService, InstanceInfoService, RankService, DayOnlyDatePipePipe, DayHourMinutePipe,
    {
      provide: InjectableRxStompConfig,
      useValue: rxStompConfig
    },
    {
      provide: RxStompService,
      useFactory: rxStompServiceFactory,
      deps: [InjectableRxStompConfig]
    },
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy
    }],
  bootstrap: [AppComponent],
  exports: [RouterModule]
})
export class AppModule {
  constructor(library: FaIconLibrary) {
    library.addIcons(faInfoCircle, faArrowLeft);
  }
}
