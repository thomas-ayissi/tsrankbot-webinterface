export const environment = {
  production: true,
  rankBotApiUrl: '/api/',
  statsDBApiUrl: 'https://statsdb-api.siege-insights.net/',
  websocketUrl: 'wss://' + window.location.hostname + '/jsa-stomp-endpoint/'
};
