# TSRankBot Webinterface (Angular, Bootstrap
## Introduction
This is a webinterface for the [TSRankBot](https://gitlab.com/siege-insights/ts3-bot) written in
[Angular](https://angular.io/).

This tool is providing a stats overview for all clients which are connected to the teamspeak.
In order to show clients here, they need to have their UPlay account somehow linked to the teamspeak account.
There are multiple ways to archive such a link.
Please have a look at the [TSRankBot Server](https://gitlab.com/siege-insights/ts3-bot)  for more details.

## See it in action
**This site is available at [stats.siege-insights.net](https://stats.siege-insights.net)**

## API communication
```mermaid
graph TD;
  WebClient-->|Connects via HTTP |RankBot-Server;
  WebClient-->|Connects via WebSocket |RankBot-Server;
  RankBot-Server-->|Connects via TSQuery|Teamspeak-Server;
  WebClient-->|Connects via HTTP |StatsDB-Server;
  StatsDB-Server-->|Connects via HTTP and caches data |Ubisoft-API-Relay;
  StatsDB-Server -->|Stores data |MariaDB;
  Ubisoft-API-Relay-->|Fetches data from |Ubisoft-APIs;
```
## Installation
### Production server
This is expected to run on an apache / nginx which is providing the api via mod_proxy at `/api/` of the document root (base).

## Configuration
### With Apache
If you want to use apache to serve the frontend files and act as a proxy for the backend,
there is an [example configuration](config/apache2/apache_example_vhost_config.conf).



## Development
## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.


## License
GPLv3 see [LICENSE](LICENSE)
